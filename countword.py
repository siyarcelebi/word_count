import sys
import time

start_time = time.time()


def main():
    print ('-----------------------------------------------------------------------------------------------------\n')
    print ('WARNING ! THE FILE YOU WANT TO COUNT THE WORDS IN IT, MUST BE IN THE SAME DIRECTORY WITH THE PROGRAM.\n')
    print ('-----------------------------------------------------------------------------------------------------\n')
    counted_list = []
    freq = []
    total_arguments = len(sys.argv) - 1

    print ("There are %s files that you added.\n" % total_arguments)

    for item in range(1, total_arguments):
        file_path = str(sys.argv[item])
        read_list = file('%s' % file_path, 'r').read().split()

        for word in read_list:

            if word not in counted_list:

                counted_list.append(word)
                freq.append(1)

            else:

                index = counted_list.index(word)
                freq[index] += 1

    print("\nLooking into all the files. Please wait for the results.. \n")
    print ('------------------------------------------------------------------------------------------------\n')

    for i in range(len(counted_list)):
        print counted_list[i] + ' : ' + str(freq[i])

    print ('\n\t\t -THE END- \t\t\t\n')
    print ('------------------------------------------------------------------------------------------------\n')


main()
print("--- %s seconds ---" % (time.time() - start_time))
